var content = {
    steps: [
        {
            type: "cjaf-label-drag",
            header: cjaf.elements.subheader("Разделите вещества, входящие в состав нефти, по агрегатному состоянию."),
            content: {
                labels: [
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>CH<sub>4</sub></b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Метан"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>C<sub>6</sub>H<sub>14</sub></b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Гексан"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>C<sub>6</sub>H<sub>12</sub></b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Циклогесан"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>C<sub>6</sub>H<sub>6</sub></b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Бензол"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>C<sub>8</sub>H<sub>18</sub></b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Алканы"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>C<sub>16</sub>H<sub>34</sub></b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Гексадекан"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>C<sub>18</sub>H<sub>38</sub></b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Парафины"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>SiO<sub>2</sub></b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Диоксид кремния"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>H<sub>2</sub>S</b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Сероводород"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>H<sub>2</sub>O</b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Вода"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>N<sub>2</sub></b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Азот"
                        }
                    ],
                    [
                        {
                            _tag: "div",
                            _class: "yin1-step1-label-drag-label cjaf-relative  cjaf-cursor-pointer cjaf-center cjaf-inline cjaf-main-color cjaf-main-border-color cjaf-text cjaf-text-font",
                            _html: "<b>CH<sub>3</sub>SH</b>"
                        },
                        {
                            _tag: "span",
                            _class: "yin1-step1-label-drag-label-caption cjaf-text-color cjaf-text cjaf-text-font cjaf-inline",
                            _html: "Метантиол"
                        }
                    ]
                ],
                traps: [
                    [
                        {
                            _tag: 'span',
                            _class: 'yin1-step1-label-drag-trap-caption cjaf-text cjaf-text-font cjaf-absolute',
                            _html: 'Газы'
                        },
                        {
                            _tag: 'img',
                            _class: 'yin1-step1-label-drag-trap-delimiter cjaf-absolute',
                            _src: 'res/4.png'
                        },
                        {
                            _tag: 'div',
                            _id: 'yin1-step1-label-drag-trap-img-1',
                            _class: 'cjaf-absolute yin1-step1-label-drag-trap'
                        }
                    ],
                    [
                        {
                            _tag: 'span',
                            _class: 'yin1-step1-label-drag-trap-caption cjaf-absolute cjaf-text cjaf-text-font ',
                            _html: 'Жидкости'
                        },
                        {
                            _tag: 'img',
                            _class: 'yin1-step1-label-drag-trap-delimiter cjaf-absolute',
                            _src: 'res/4.png'
                        },
                        {
                            _tag: 'div',
                            _id: 'yin1-step1-label-drag-trap-img-2',
                            _class: 'cjaf-absolute yin1-step1-label-drag-trap'
                        }
                    ],
                    [
                        {
                            _tag: 'span',
                            _class: 'yin1-step1-label-drag-trap-caption cjaf-text cjaf-text-font cjaf-absolute',
                            _html: 'Твердые'
                        },
                        {
                            _tag: 'div',
                            _id: 'yin1-step1-label-drag-trap-img-3',
                            _class: 'cjaf-absolute yin1-step1-label-drag-trap'
                        }
                    ]

                ]
            },
            settings: {
                set: {
                    start: "random",
                    correct: [
                        [0, 8, 10, 11],
                        [1, 2, 3, 4, 9],
                        [5, 6, 7]
                    ],
                },
                groupOrdered: false,
                clearTraps: false
            }
        }
    ],
    header: cjaf.elements.header("Состав нефти"),
    validator: {
        button: {
            restart: [
                {
                    _class: "yin1-validator-restart-img cjaf-absolute cjaf-anchor-top cjaf-anchor-right cjaf-cursor-pointer",
                    _tag: "div"
                }
            ],
            validate: cjaf.elements.textButton("Проверить"),
            continue: cjaf.elements.textButton("Продолжить")
        },
        window: {
            modal: {
                text: cjaf.elements.modalWindowText(
                    "Правильно!",
                    "Задание выполнено верно. Теперь можно продолжить или попробовать еще раз."
                ),
                button: {
                    restart: cjaf.elements.textButton("Начать заново", true)
                }
            },
            popUp: {
                text: {
                    wrong: cjaf.elements.popUpWindowText("Найдены некоторые ошибки. Попробуй еще раз.", true),
                    correct: cjaf.elements.popUpWindowText("Все верно!")
                }
            }
        }
    }
};

let yin1_app = new Vue({
    el: "#app",
    data: {
        content: content
    },
    mounted: function() {
        preloadImages([
            "res/1h.png",
            "res/2h.png",
            "res/3h.png"
        ]);
    }
});