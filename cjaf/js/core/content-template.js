

var base = [
    {
        _tag: "h1"
    }
];


var step = [
    {
        content: [],
        header: base,
        settings: {},
        type: ""
    }
];


var taskContent = {
    steps: [
        step
    ],
    header: [

    ],
    validator: {
        button: {
            restartImg: base,
            restart: base
        },
        window: {
            modal: {
                text: {
                    wrong: base,
                    connect: base
                },
                button: {
                    restart: base
                }
            },
            popup: {
                text: base
            }
        }
    }
};