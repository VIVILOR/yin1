Vue.component('cjaf-label-swap', {
    template: '\
         <div\
             :class="prefix + \'-label-swap-wrapper\'"\
             :id="prefix + \'-label-swap\'"\
         >\
             <div\
                 v-for  = "i in content.length"\
                 :class = "prefix + \'-label-swap-pair\'"\
                 :style = "settings.styles ? settings.styles[i-1] : \'\'"\
             >\
                 <cjaf-base\
                     :class         ="labelClass"\
                     :content       ="content[i-1][currentSet[i-1]]"\
                     :correct       ="currentSet[i-1] === correctSet[i-1]"\
                     :highlight     ="highlight"\
                     :prefix        ="prefix"\
                     :show-tag      ="true"\
                     @click.native  ="swapLabels(i-1)"\
                 >\
                 </cjaf-base>\
                 <cjaf-base\
                     v-if           ="settings.delimiter"\
                     :class         ="prefix + \'-label-delimiter\'"\
                     :content       ="settings.delimiter"\
                     :prefix        ="prefix"\
                     @click.native  ="swapLabels(i-1)"\
                 >\
                 </cjaf-base>\
                 <cjaf-base\
                     :class         ="labelClass"\
                     :content       ="content[i-1][!currentSet[i-1] | 0]"\
                     :correct       ="currentSet[i-1] === correctSet[i-1]"\
                     :highlight     ="highlight"\
                     :prefix        ="prefix"\
                     :show-tag      ="true"\
                     @click.native  ="swapLabels(i-1)"\
                 >\
                 </cjaf-base>\
             </div>\
         </div>',
    props: {
        content: {
            type: Array,
            required: true
        },
        settings: {
            type: Object,
            default: function() {
                return {
                    styles: [],
                    delimiter: null
                }
            }
        },
        prefix: {
            type: String,
            default: 'cjaf'
        }
    },
    data: function() {
        return {
            completed: false,
            correctSet: cjaf.utils.fillArray({
                len: this.content.length,
                maxValue: 1
            }),
            currentSet: cjaf.utils.fillArray({
                len: this.content.length,
                isBinary: true
            }),
            highlight: false
        }
    },
    computed: {
        labelClass: function() {
            var s = {};
            s[this.prefix + '-label-default'] = true;
            s[this.prefix + '-label-validating'] = this.highlight;
            return s;
        }
    },
    methods: {
        validate: function() {
            this.completed = cjaf.utils.compareArrays({
                current: this.currentSet,
                isBinary: true
            });
            this.highlight = true;
            this.$emit('validationComplete', this.completed);
        },
        reset: function() {
            this.completed = false;
            this.currentSet = cjaf.utils.fillArray({
                len: this.content.length,
                isBinary: true
            });
            this.highlight = false;
        },
        swapLabels: function(i) {
            Vue.set(this.currentSet, i, !this.currentSet[i] | 0);
        }
    }
});

