Vue.component('cjaf-label-drag', {
    template: '\
    <div\
        :class="[extendedPrefix + \'-wrapper\', \'cjaf-relative\']"\
        :id="extendedPrefix">\
        <div :class="extendedPrefix + \'-labels-wrapper\'">\
            <cjaf-base\
                v-for       ="i in content.labels.length"\
                :id         ="extendedPrefix + \'-label-\' + (i-1)"\
                :class      ="labelVisibilityControl(i-1, false)"\
                :ref        ="extendedPrefix + \'-label-\' + (i-1)"\
                :content    ="content.labels[params.dynamic.labels.contentIndex[i-1]]"\
                :highlight  ="highlight"\
                :prefix     ="extendedPrefix"\
                :show-tag   ="true">\
            </cjaf-base>\
        </div>\
        <div\
            :class="extendedPrefix + \'-traps-wrapper\'">\
            <cjaf-base\
                v-for   ="(trap, i) in content.traps"\
                :id     ="extendedPrefix + \'-trap-\' + i"\
                :class  ="extendedPrefix + \'-trap-wrapper\'"\
                :content="trap">\
            </cjaf-base>\
            <cjaf-base\
                v-for       ="i in content.labels.length"\
                :id         ="extendedPrefix + \'-trapped-label-\' + (i-1)"\
                :class      ="labelVisibilityControl(i-1, true)"\
                :correct    ="params.dynamic.trappedLabels.validity[i-1]"\
                :content    ="content.labels[params.dynamic.trappedLabels.contentIndex[i-1] !== -1 ?\
                    params.dynamic.trappedLabels.contentIndex[i-1] : 0\
                ]"\
                :highlight  ="highlight"\
                :prefix     ="extendedPrefix"\
                :show-tag   ="true">\
            </cjaf-base>\
        </div>\
    </div>\
    ',
    props: {
        content: {
            type: Object,
            required: true
        },
        settings: {
            type: Object,
            required: true
        },
        prefix: {
            type: String,
            default: 'cjaf'
        }
    },
    data: function() {
        let _this           = this;
        let correctSet      = this.settings.set.correct;
        let trapsCount      = 0;
        let groupsOffsets   = [];
        let groupsLengths   = [];
        let labelsResetFunction         = null;
        let labelsResetFunctionParams   = null;
        
        for (let i = 0; i < correctSet.length; i++) {
            groupsOffsets.push(trapsCount);
            groupsLengths.push(correctSet[i].length);
            
            trapsCount += correctSet[i].length;
        }
        
        switch (this.settings.set.start) {
            case "random":
                console.log("random");
                labelsResetFunction = cjaf.utils.fillShuffledArray;
                labelsResetFunctionParams = {
                    len: this.content.labels.length
                };
                break;
            case "default":
            default:
                console.log("default");
                labelsResetFunction = cjaf.utils.fillArray;
                labelsResetFunctionParams = {
                    len: this.content.labels.length,
                    ordered: true
                };
                break;
        }
        
        return {
            params: {
                static: {
                    /* Type: Number
                     * Length of source array of label content
                     */
                    labelsCount:            _this.content.labels.length,
            
                    /* Type: Number
                     * Length of array of trapped labels. It can be either more than labelsCount or less or equal.
                     */
                    trappedLabelsCount:     trapsCount,
            
                    /* Type: Array
                     * Index offsets of each traps group
                     */
                    trapsGroupsOffsets:     groupsOffsets,
            
                    /* Type: Array
                     * Number of traps in each traps group
                     */
                    trapsGroupsLengths:     groupsLengths,
            
                    /* Type: Boolean
                     * UX flag. Switches visual reflection of dragging label from start positions. If labels are not
                     * unique, each label can be trapped by different traps, or, may be, different traps' groups. If
                     * they are unique each label can be placed only in one trap, or place in traps' group.
                     */
                    areLabelsUnique:        _this.content.labels.length < _this.content.traps.length,
            
                    /* Type: Boolean
                     * UX flag. Groups can be detected if content.settings.set.correct contants more than one element:
                     */
                    areGroupsPresent:       _this.settings.set.correct.length > 1,
            
                    /* Type: Boolean
                     * Logical flag. If this flag is set 'true' validator should check the order of trapped labels.
                     */
                    groupOrdered:           _this.settings.groupOrdered,
                    
                    /* Type: Function
                     * Sets the intial state and state after reset for labels' positions.
                     */
                    labelsResetFunction:   labelsResetFunction,
    
                    /* Type: Object
                     * Parameters to pass to the function above
                     */
                    labelsResetFunctionParams:  labelsResetFunctionParams,
                    
                    /* Type: Boolean
                     * If this paramter set true, labels will stay in traps
                     */
                    clearTraps:             _this.settings.clearTraps
                },
                dynamic: {
                    labels: {
                        contentIndex:   null,
                        displayState:   null,
                
                        hide: function(i) {
                            Vue.set(this.displayState, i, false);
                        },
                
                        show: function(i) {
                            Vue.set(this.displayState, i, true);
                        },
                        
                        reset: function() {
                            this.contentIndex = _this.params.static.labelsResetFunction(
                                _this.params.static.labelsResetFunctionParams
                            );
                            this.displayState = cjaf.utils.fillArray({
                                len: _this.params.static.labelsCount,
                                value: true
                            });
                        }
                    },
            
                    trappedLabels: {
                        validity:       null,
                        contentIndex:   null,
                        displayState:   null,
                        startPositions: null,
                
                        hide: function(i) {
                            Vue.set(this.displayState, i, false);
                        },
                
                        show: function(i) {
                            Vue.set(this.displayState, i, true);
                        },
                
                        swap: function(i, j) {
                            this.contentIndex.vueSwap(i, j);
                            this.startPositions.vueSwap(i, j);
                        },
                
                        resetTrap: function(i) {
                            this.contentIndex[i]    = -1;
                            this.startPositions[i]  = -1;
                        },
                
                        setValid: function(i) {
                            Vue.set(this.validity, i, true);
                        },
                        
                        reset: function() {
                            let trappedLabelsCount = _this.params.static.trappedLabelsCount;
                            
                            this.validity =         cjaf.utils.fillArray({ len: trappedLabelsCount, value: false });
                            this.contentIndex =     cjaf.utils.fillArray({ len: trappedLabelsCount, value: -1 });
                            this.displayState =     cjaf.utils.fillArray({ len: trappedLabelsCount, value: false });
                            this.startPositions =   cjaf.utils.fillArray({ len: trappedLabelsCount, value: -1 });
                        }
                    },
            
                    draggingLabel: {
                        contentIndex:   -1,
                        positionIndex:  -1,
                        fromTrap:       false,
                
                        set: function(params) {
                            let index = params.index;
                    
                            this.positionIndex = index;
                            this.fromTrap = !!params.fromTrap;
                            
                            if (params.fromTrap) {
                                let trappedLabels = _this.params.dynamic.trappedLabels;
                                this.contentIndex = trappedLabels.contentIndex[index];
                                trappedLabels.hide(index);
                            } else {
                                let labels = _this.params.dynamic.labels;
                                this.contentIndex = labels.contentIndex[index];
                                labels.hide(index);
                            }
                        },
                
                        move: function(params) {
                            if (!params) params = {};
                            let index =         params.index;
                            let labels =        _this.params.dynamic.labels;
                            let trappedLabels = _this.params.dynamic.trappedLabels;
                    
                            if (this.fromTrap) {
                                if (params.toLabel) {
                                    if(trappedLabels.contentIndex[index] !== -1) {
                                        trappedLabels.show(this.positionIndex);
                                    }
                                    
                                    trappedLabels.show(index);
                                    trappedLabels.swap(this.positionIndex, index);
                                } else if (params.toGroup) {
                                    let freeTrapIndex = _this.getGroupIndexes(index)[0];
                            
                                    if (freeTrapIndex === undefined) {
                                        trappedLabels.show(this.positionIndex);
                                    } else {
                                        this.move({
                                            index: freeTrapIndex,
                                            fromTrap: true,
                                            toLabel: true
                                        });
                                    }
                                } else {
                                    let labelIndex = trappedLabels.startPositions[this.positionIndex];
                            
                                    trappedLabels.hide(this.positionIndex);
                                    trappedLabels.resetTrap(this.positionIndex);
                            
                                    // TODO: Add animation of label return
                                    labels.show(labelIndex);
                                }
                            } else {
                                if (params.toLabel) {
                                    if(trappedLabels.contentIndex[index] !== -1) {
                                        labels.show(trappedLabels.startPositions[index]);
                                    }
                                    
                                    trappedLabels.startPositions[index] = this.positionIndex;
                                    trappedLabels.contentIndex[index] = this.contentIndex;
    
                                    // TODO: Add animation
                                    trappedLabels.show(index);
                                } else if (params.toGroup) {
                                    // TODO (optional): Add random choose of group index
                                    let freeTrapIndex = _this.getGroupIndexes(index)[0];
                            
                                    if (freeTrapIndex === undefined) {
                                        labels.show(this.positionIndex);
                                    } else {
                                        this.move({
                                            index: freeTrapIndex,
                                            fromTrap: false,
                                            toLabel: true
                                        });
                                    }
                                } else {
                                    // TODO: Add animation
                                    labels.show(this.positionIndex);
                                }
                            }
                        }
                    }
                }
            },
            completed: false,
            highlight: false,
            extendedPrefix: this.prefix + '-label-drag'
        }
    },
    methods: {
        validate: function() {
            this.completed = this.checkValidity();
            this.highlight = true;
            this.$emit('validationComplete', this.completed);
        },
        
        reset: function(isGlobal) {
            console.log("Is global: " + isGlobal);
            let labels = this.params.dynamic.labels;
            let trappedLabels = this.params.dynamic.trappedLabels;
            
            this.completed = false;
            
            if (isGlobal) {
                labels.reset();
                trappedLabels.reset();
            } else {
                if (this.params.static.clearTraps) {
                    labels.reset();
                    trappedLabels.reset();
                } else {
                    if (labels.contentIndex === null) {
                        labels.reset();
                    }
                    if (trappedLabels.contentIndex === null) {
                        trappedLabels.reset();
                    }
                }
            }
            
            this.highlight = false;
        },
        
        checkValidity: function() {
            let k = 0,
                correct = this.settings.set.correct,
                trapped = this.params.dynamic.trappedLabels,
                isGroupOrdered = this.settings.set.groupOrdered;
            
            for (let i = 0; i < correct.length; i++) {
                for (let j = 0; j < correct[i].length; j++) {
                    
                    if (isGroupOrdered) {
                        if (correct[i][j] === trapped.contentIndex[k]) {
                            trapped.setValid(k);
                        }
                    } else {
                        if (correct[i].includes(trapped.contentIndex[k])) {
                            trapped.setValid(k);
                        }
                    }
                    k++;
                }
            }
            return !trapped.validity.includes(false);
        },
        
        getGroupIndexes: function(index) {
            let ret = [],
                offsets = this.params.static.trapsGroupsOffsets,
                lengths = this.params.static.trapsGroupsLengths,
                trapped = this.params.dynamic.trappedLabels.contentIndex;
            
            for (let i = offsets[index]; i < offsets[index] + lengths[index]; i++) {
                if (trapped[i] === -1) {
                    ret.push(i);
                }
            }
            
            return ret;
        },
        
        labelVisibilityControl: function(i, isTrappedLabel) {
            let ret = {};
            ret['cjaf-relative'] = true;
            ret[this.extendedPrefix + (isTrappedLabel ? '-trapped' : '') + '-label-wrapper'] = true;
            ret[this.extendedPrefix + (isTrappedLabel ? '-trapped' : '') + '-label-wrapper-hidden']
                = !this.params.dynamic[isTrappedLabel ? 'trappedLabels' : 'labels'].displayState[i];
            
            return ret;
        }
    },
    
    created: function() {
        this.reset(true);
    },
    
    mounted: function() {
        let _this = this;
        let selectorPrefix = '.' + this.extendedPrefix;
        
        let labelSelector =
            selectorPrefix + '-label-wrapper ' +
            selectorPrefix + '-label';
        
        let trappedLabelSelector =
            selectorPrefix + '-trapped-label-wrapper ' +
            selectorPrefix + '-label';
        
        let trapSelector =
            selectorPrefix + '-trap';
        
        $(labelSelector)
            .draggable({
                helper: "clone",
                zIndex: 1000,
                appendTo: "body",
                revert: function(isValidDroppable) {
                    if (!isValidDroppable) {
                        _this.params.dynamic.draggingLabel.move();
                        return true;
                    }
                },
                revertDuration: 0,
                start: function(event) {
                    let labelId = event.target.parentElement.id;
                    
                    $(trapSelector).droppable("enable");
                    _this.params.dynamic.draggingLabel.set({
                        index: cjaf.utils.getLabelIndexFromId(labelId)
                    });
                }
            });
        
        $(trappedLabelSelector)
            .draggable({
                helper: "clone",
                zIndex: 1000,
                appendTo: "body",
                revert: function(isValidDroppable) {
                    if (!isValidDroppable) {
                        _this.params.dynamic.draggingLabel.move({
                            fromTrap: true
                        });
                        return true;
                    }
                },
                revertDuration: 0,
                start: function(event) {
                    let labelId = event.target.parentElement.id;
        
                    $(trapSelector).droppable("enable");
                    _this.params.dynamic.draggingLabel.set({
                        index: cjaf.utils.getLabelIndexFromId(labelId),
                        fromTrap: true
                    });
                }
            });
        
        $(trappedLabelSelector)
            .droppable({
                tolerance: "pointer",
                greedy: false,
                drop: function(event) {
                    let labelIndex = event.target.parentElement.id;
    
                    $(trapSelector).removeClass("ui-droppable-hover");
                    _this.params.dynamic.draggingLabel.move({
                        index: cjaf.utils.getLabelIndexFromId(labelIndex),
                        toLabel: true
                    });
                },
                over: function() {
                    // TODO: Here must be the animation
                    $(trapSelector).droppable("disable");
                },
                out: function() {
                    // TODO: Here must be the animation
                    $(trapSelector).droppable("enable");
                }
            });
        
        $(trapSelector)
            .droppable({
                tolerance: "pointer",
                greedy: false,
                drop: function(event) {
                    let labelIndex = event.target.parentElement.id;
                    
                    _this.params.dynamic.draggingLabel.move({
                        index: cjaf.utils.getLabelIndexFromId(labelIndex),
                        toGroup: true
                    });
                }
            });
    }
});

