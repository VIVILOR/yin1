Array.prototype.vueSwap = function (x, y) {
    var b = this[x];
    Vue.set(this, x, this[y]);
    Vue.set(this, y, b);
    return this;
};

function preloadImages(array) {
    if (!preloadImages.list) {
        preloadImages.list = [];
    }
    var list = preloadImages.list;
    for (var i = 0; i < array.length; i++) {
        var img = new Image();
        img.onload = function() {
            var index = list.indexOf(this);
            if (index !== -1) {
                // remove image from the array once it's loaded
                // for memory consumption reasons
                list.splice(index, 1);
            }
        };
        list.push(img);
        img.src = array[i];
    }
}

var cjaf = {
    prefix: "cjaf",
    mixins: {},
    utils: {
        compareArrays: function (ops) {
            var i = 0;

            if (ops.isBinary) {
                console.log('Binary array');
                for (i = 0; i < ops.current.length - 1; i++) {
                    if (ops.current[i] !== ops.current[i + 1]) {
                        console.log('c');
                        return false;
                    }
                }
                return true;
            } else {
                if (ops.correct) {
                    for (i in ops.current)
                        if (ops.current[i] !== ops.correct[i])
                            return false;
                    return true;
                }
            }
            return false;
        },

        fillArray: function (ops) {

            if (!ops || !ops.len)
                return null;

            var ret = [];
            var i = 0;
            var divider = null;

            ret.length = ops.len;
            if (ops.ordered) {
                var d = (ops.init ? ops.init : 0);
                for (i = 0; i < ops.len; i++) {
                    ret[i] = d + i;
                }
                return ret;
            }
            if (ops.value !== undefined) {
                ret.fill(ops.value);
                return ret;
            }

            if (ops.isBoolean) {
                ret.fill(false);
                return ret;
            }

            if (ops.maxValue)
                divider = ops.maxValue;
            else if (ops.isBinary)
                divider = 2;
            else
                divider = 10;

            for (i = 0; i < ret.length; i++) {
                ret[i] = (Math.random() * 100 | 0) % divider;
                if (i === ret.length - 1) {
                    if (this.compareArrays({
                        correct: ops.correct,
                        current: ret,
                        isBinary: ops.isBinary
                    })) {
                        i = 0;
                    }
                }
            }

            return ret;
        },

        shuffleArray: function(ret) {
            for (i = ret.length; i; i--) {
                j = Math.floor(Math.random() * i);
                x = ret[i - 1];
                ret[i - 1] = ret[j];
                ret[j] = x;
            }
        },

        fillShuffledArray: function(ops) {
            if (!ops || !ops.len)
                return null;

            var ret = [], k;

            ret.length = ops.len;

            for (k = 0; k < ret.length; k++) {
                ret[k] = k;
            }
            
            if (!this.shuffleArray) {
                cjaf.utils.shuffleArray(ret);
            } else {
                this.shuffleArray(ret);
            }
            return ret;
        },
        getLabelIndexFromId: function(id) {
            return id.match(/[0-9]+$/g)[0] | 0;
        },
        countDistance: function(A, B) {
            var aX = A.left,
                aY = A.top,
                bX = B.left,
                bY = B.top;

            return Math.sqrt(
                Math.pow(aX - bX, 2) - Math.pow(aY - bY, 2)
            );
        }
    },
    elements: {
        header: function(html) {
            return [
                {
                    _class: "cjaf-header cjaf-header-font",
                    _html: html,
                    _tag: "h1"
                }
            ]
        },
        subheader: function(html) {
            return [
                {
                    _class: "cjaf-subheader cjaf-subheader-font",
                    _html: html,
                    _tag: "h2"
                }
            ]
        },
        textButton: function(html, light) {
            return [
                {
                    _class: "\
                        cjaf-button\
                        cjaf-button-text\
                        cjaf-button-text-font\
                        cjaf-center\
                        cjaf-cursor-pointer\
                        cjaf-inline\
                        cjaf-transition\
                        cjaf-button" +
                    (light ? "-light" : "-normal"),
                    _html: html,
                    _tag: "div"
                }
            ]
        },
        popUpWindowText: function(html, wrong) {
            return [
                {
                    _class: "\
                        cjaf-text\
                        cjaf-text-font\
                        cjaf-padding\
                    " + (wrong ? "cjaf-error-text-color" : "cjaf-text-color"),
                    _html: html,
                    _tag: "div"
                }
            ]
        },
        modalWindowText: function(header, text) {
            return [
                {
                    _tag:   'h2',
                    _class: '\
                        cjaf-subheader\
                        cjaf-header-font\
                        cjaf-main-color\
                        cjaf-padding-top\
                        ',
                    _html:  header
                },
                {
                    _tag:   'div',
                    _class: '\
                        cjaf-text\
                        cjaf-interactive-text-font\
                        cjaf-text-color\
                        cjaf-padding-top\
                        cjaf-padding-bottom\
                    ',
                    _html:  text,
                    _style: 'line-height: 24px'
                }
            ]
        }
    }
};